///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
///
/// @file gge.cpp
/// @version 1.0
///
/// @author Christian Li <lichrist@hawaii.edu>
/// @date   13_Feb_2022
///////////////////////////////////////////////////////////////////////////////
//

#include "gge.h"

double fromGasToJoule( double gas){
   return gas / GASOLINE_GALLONS_IN_A_JOULE;
}


double fromJouleToGas( double joule){
   return joule * GASOLINE_GALLONS_IN_A_JOULE;
}

