/////////////////////////////////////////////////////////////////////////////
////
///// University of Hawaii, College of Engineering
///// @brief Lab 05b - CatPower 2 - EE 205 - Spr 2022
/////
///// @file cat.cpp
///// @version 1.0
/////
///// @author Christian Li <lichrist@hawaii.edu>
///// @date 13_Feb_2022
///////////////////////////////////////////////////////////////////////////////
//

#include "cat.h"

double fromCatPowerToJoule( double catPower){
   //Cat will return nothing
   return 0.0; 
}

double fromJouleToCatPower( double joule){
   //Cat will return nothing
   return 0.0;
}
